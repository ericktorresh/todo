<!DOCTYPE html>
<html>
	<head>
		<title>ToDo</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1, user-scalable=yes">
		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="yes">

		<link rel="stylesheet" href="assets/css/main.css">
		<script src="assets/bower/webcomponentsjs/webcomponents-lite.min.js"></script>
		<script src="assets/bower/jquery/dist/jquery.min.js" ></script>
		<script src="assets/bower/moment/min/moment.min.js"></script>

		<link rel="import" href="webcomponents/all-imports.html">

	</head>
	<body unresolved class="fullbleed layout vertical">
		<div class="flex">
			<paper-toolbar>
				<div class="title">Todo App</div>
				<!-- Icons -->
				<paper-icon-button icon="refresh"></paper-icon-button>
				<paper-icon-button icon="search"></paper-icon-button>
			</paper-toolbar>

			<!-- Main Content -->
			<div class="content">
				<task-box-element source="/task.json"></task-box-element>
			</div>
		</div>

		<script type="text/javascript">

		</script>
	</body>
</html>
